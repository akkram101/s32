const express=require(`express`);
const {
	register,
	getAllUsers,
	checkEmail,
	login
}=require(`./../controllers/userControllers`)
const router = express.Router()



//REGISTER A USER
router.post(`/register`, async (req,res)=>{
	console.log(req.body)
	try{
		await register(req.body).then(response => res.send(response))
	} catch(err){
		res.status(500).json(err)
	}
})

//GET ALL USERS
router.get('/', async (req, res) => {

	try{
		await getAllUsers().then(result => res.send(result))

	}catch(err){
		res.status(500).json(err)
	}
})

//CHECK IF EMAIL ALREADY EXISTS
router.post(`/email-exists`, async (req, res)=>{
	try{
		await checkEmail(req.body.email).then(result => res.send(result))
	}catch(error){
		res.status(500).json(error)
	}
})

//LOGIN USER
router.post(`/login`, async (req,res) => {
	try{
		await login(req.body).then(result => res.send(result))
	} catch(error) {
		res.status(500).json(error)
	}
})



module.exports=router;
