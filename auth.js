const jwt= require(`jsonwebtoken`);

//sign
module.exports.createToken = (data) =>{

	let userData={
		id:data._id,
		email:data.email,
		isAdmin: data.isAdmin
	}

	//creates the token
	return jwt.sign (userData, process.env.SECRET_PASS);
}


//verify